<?php 
include("functions/safe.php");
include("functions/goLink.php");
if(isset($_COOKIE['user']) and isset($_COOKIE['password']) and is_file("users/admin/login.php")){
  $user=$_COOKIE['user'];
  if(is_dir("users/".safeDir($user))){
    include("users/".safeDir($user)."/login.php");
    if(is_file("users/".safeDir($user)."/permission.php")){
      include("users/".safeDir($user)."/permission.php");
    }else{
      $usertype=2;
    }
  }else{
    include("logout.php");
    goTemplate("no_user");
    exit();
  }
  unset($passwrd);
}else{
  go("login.php");
}

?>
