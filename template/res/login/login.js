function viewpass(){
  var pass = document.getElementById('pass');
  var box = document.getElementById('passbox');
  if(box.type == 'password'){
    box.type = 'text';
    pass.src = '/template/res/login/hidepass.png';
  } else {
    box.type = 'password';
    pass.src = '/template/res/login/showpass.png';   
  }
}